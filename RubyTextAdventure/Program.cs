﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RubyTextAdventure
{
    class Program
    {
        static void Main(string[] args)
        {
            Game g = new Game();
            g.Go("startup.rb");
            while(true)
            {
                dynamic a = g.Execute(Console.ReadLine());
                if (a is string || a is IronRuby.Builtins.MutableString)
                    Console.WriteLine(a);
                else
                    Console.WriteLine("What?");
            }
        }
    }
}
