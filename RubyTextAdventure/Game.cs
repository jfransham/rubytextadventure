﻿using IronRuby;
using Microsoft.Scripting.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RubyTextAdventure
{
    public class Game
    {
        private TextWriter Out { get { return Runtime.IO.OutputWriter; } }
        private MemoryStream OutStream { get; set; }
        private TextReader In { get { return Runtime.IO.InputReader; } }
        private MemoryStream InStream { get; set; }

        public ScriptRuntime Runtime { get { return Engine.Runtime; } }
        public ScriptEngine Engine { get { return Scope.Engine; } }
        public ScriptScope Scope { get; private set; }

        public void Go()
        {
            Go(null);
        }

        public void Go(string inputpath)
        {
            Init(inputpath);
        }

        public dynamic Execute(string input)
        {
            try
            {
                return Engine.Execute(input, Scope);
            }
            catch(Exception e)
            {
                return "I don't know how to do that";
            }
        }

        private void Init(string path)
        {
            OutStream = new MemoryStream();
            InStream = new MemoryStream();

            LanguageSetup s = Ruby.CreateRubySetup();
            ScriptEngine eng = Ruby.CreateEngine();
            eng.Runtime.IO.SetOutput(OutStream, Encoding.ASCII);
            eng.Runtime.IO.SetInput(InStream, Encoding.ASCII);

            Scope = path == null ? eng.CreateScope() : eng.ExecuteFile(path);
        }

        ~Game()
        {
            OutStream.Dispose();
            InStream.Dispose();
        }
    }
}
