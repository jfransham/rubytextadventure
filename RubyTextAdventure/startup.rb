Main = self

class HalfOnDef
	def initialize(inner)
		@inner = inner
	end
	
	def inner
		@inner
	end
end

class OnDef
	def initialize(innera, innerb)
		@innera = innera
		@innerb = innerb
	end
	
	def inner_left
		@innera
	end
	
	def inner_right
		@innerb
	end
end

def the *args
	return args[0]

	if args.length > 0
		return TheDef.new args[0]
	end
	
	raise "too many args"
end

def on *args
	if args.length > 0
		return HalfOnDef.new args[0]
	end
	
	raise "too many args"
end

def unpackhalfon(outer)
	if outer.is_a?(HalfOnDef)
		return outer.inner
	end
	
	raise "incorrect type"
end

def unpackthe(outer)
	if outer.is_a?(TheDef)
		return outer.inner
	end
	
	outer
end

def place *args
	if args.length > 0
		return "Placed #{args[0].inner_left} on #{args[0].inner_right}"
	end
	
	raise "place what?"
end
alias put place

def noun(n, *args)
	if args.length > 0
		return OnDef.new(n, unpackhalfon(args[0]))
	end
	
	n
end

def dog *args; noun(:dog, *args); end
def stairs *args; noun(:stairs, *args); end